package org.rl4o.provider.environment.bandit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.rl4o.api.datalogger.DataLoggerBuilder;
import org.rl4o.api.environment.EnvironmentName;
import org.rl4o.api.environment.RLEnvironment;

@Designate(ocd = NormalKArmedBanditEnvironment.Config.class, factory = true)
@EnvironmentName("normal-k-armed-bandit")
@Component(scope = ServiceScope.PROTOTYPE)
public class NormalKArmedBanditEnvironment implements RLEnvironment
{
  @ObjectClassDefinition
  public @interface Config
  {
    public int k() default 10;
  }

  @Activate
  private Config config;

  @Reference
  private DataLoggerBuilder dataLoggerFactory;

  private List<Random> randoms;
  private List<Double> means;

  @Override
  public Integer getState()
  {
    return config.k();
  }

  @Override
  public Double execute(Integer action)
  {
    return means.get(action) + randoms.get(action).nextGaussian();
  }

  @Activate
  public void activate()
  {
    final Random rand = new Random();

    randoms = new ArrayList<>(config.k());
    means = new ArrayList<>(config.k());

    for (int i = 0; i < config.k(); i++)
    {
      randoms.add(new Random());
      means.add(rand.nextGaussian());
    }
  }
}
