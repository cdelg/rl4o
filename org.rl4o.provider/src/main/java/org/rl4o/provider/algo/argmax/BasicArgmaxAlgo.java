package org.rl4o.provider.algo.argmax;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.util.promise.Promise;
import org.rl4o.api.algo.AlgoName;
import org.rl4o.api.algo.RLAlgo;
import org.rl4o.api.datalogger.DataLogger;
import org.rl4o.api.datalogger.DataLoggerBuilder;
import org.rl4o.provider.worker.Worker;

@Designate(ocd = BasicArgmaxAlgo.Config.class, factory = true)
@AlgoName("basic-argmax")
@Component
public class BasicArgmaxAlgo implements RLAlgo
{
  @ObjectClassDefinition
  public @interface Config
  {
    public double epsilon() default 0.0;
  }

  @Activate
  private Config config;

  @Reference
  private DataLoggerBuilder dataLoggerBuilder;

  @Reference
  private Worker worker;

  @Override
  public Promise<Void> solve(Supplier<Integer> sensor, Function<Integer, Double> effector,
      int timeSteps)
  {
    final int k = sensor.get();

    if (effector == null)
    {
      throw new NullPointerException("effector");
    }
    if (timeSteps < 0)
    {
      throw new IllegalArgumentException("timeSteps must be greater or egual to 0");
    }
    if (k < 0)
    {
      throw new IllegalArgumentException("environment must be greater than 0");
    }

    return worker.run(cont -> solve(k, effector, cont, config.epsilon()), timeSteps);
  }

  @Override
  public AutoCloseable solve(Supplier<Integer> sensor, Function<Integer, Double> effector)
  {
    final int k = sensor.get();

    if (effector == null)
    {
      throw new NullPointerException("effector");
    }
    if (k < 0)
    {
      throw new IllegalArgumentException("timeSteps must be greater than 0");
    }

    return worker.run(cont -> solve(k, effector, cont, config.epsilon()));
  }

  private void solve(int k, Function<Integer, Double> effector, Function<Long, Boolean> cont,
      double epsilon)
  {
    final DataLogger dataLogger = dataLoggerBuilder.addColumn(Long.class, "Step")
        .addColumn(Integer.class, "Selected Action").addColumn(Double.class, "Reward").build();

    final List<Double> rewardSums = new ArrayList<>(k);
    final List<Double> actionCounts = new ArrayList<>(k);

    for (int i = 0; i < k; i++)
    {
      rewardSums.add(0.0);
      actionCounts.add(0.0);
    }

    for (long step = 0; cont.apply(step); step++)
    {
      final int selectedAction = selectAction(rewardSums, actionCounts, epsilon);
      final double reward = effector.apply(selectedAction);

      dataLogger.log(step, selectedAction, reward);

      rewardSums.set(selectedAction, rewardSums.get(selectedAction) + reward);
      actionCounts.set(selectedAction, actionCounts.get(selectedAction) + 1);
    }
  }

  private int selectAction(List<Double> rewardSums, List<Double> actionCounts, double epsilon)
  {
    final Random rand = new Random();
    int selectedAction = rand.nextInt(rewardSums.size());

    if (epsilon < rand.nextDouble())
    {
      double maxValue = 0;
      for (int i = 0; i < rewardSums.size(); i++)
      {
        final double value = rewardSums.get(i) / actionCounts.get(i);
        if (value > maxValue)
        {
          maxValue = value;
          selectedAction = i;
        }
      }
    }

    return selectedAction;
  }
}
