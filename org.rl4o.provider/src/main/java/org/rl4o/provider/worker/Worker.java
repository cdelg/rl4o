package org.rl4o.provider.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;
import org.osgi.util.promise.Deferred;
import org.osgi.util.promise.Promise;

@Component(service = Worker.class, immediate = true)
public class Worker
{
  @Reference(service = LoggerFactory.class)
  private Logger logger;

  private final ExecutorService executor = Executors.newCachedThreadPool();

  private final List<AutoCloseable> runningAlgos = new ArrayList<>();

  public Promise<Void> run(Consumer<Function<Long, Boolean>> algo, int timeSteps)
  {
    return doRun(algo, timeSteps).p;
  }

  public AutoCloseable run(Consumer<Function<Long, Boolean>> algo)
  {
    return doRun(algo, -1).c;
  }

  synchronized private Res doRun(Consumer<Function<Long, Boolean>> algo, int timeSteps)
  {
    final Deferred<Void> end = new Deferred<>();

    final AtomicBoolean cancel = new AtomicBoolean(false);

    final AutoCloseable closeable = new AutoCloseable()
    {
      @Override
      public void close() throws Exception
      {
        cancel.set(true);
        end.getPromise().timeout(60000).getValue();
      }
    };

    executor.submit(() -> {
      try
      {
        if (timeSteps != -1)
        {
          algo.accept(i -> i < timeSteps && !cancel.get());
        } else
        {
          algo.accept(i -> !cancel.get());
        }
      } catch (final Exception e)
      {
        end.fail(e);
      } finally
      {
        if (!end.getPromise().isDone())
        {
          end.resolve(null);
        }
      }
    });

    runningAlgos.add(closeable);

    final Res res = new Res();
    res.p = end.getPromise();
    res.c = closeable;

    return res;
  }

  @Deactivate
  synchronized public void deactivate() throws InterruptedException
  {
    runningAlgos.stream().forEach(a -> {
      try
      {
        a.close();
      } catch (final Exception e)
      {
        logger.error("A running algo failed on shutdown.", e);
      }
    });
    executor.shutdown();
    executor.awaitTermination(60, TimeUnit.SECONDS);
  }

  private class Res
  {
    public AutoCloseable c;
    public Promise<Void> p;
  }
}
