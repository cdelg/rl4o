# Rl4o Provider

The the Rl4o implementation bundle.

## Links

* [Documentation](https://gitlab.com/cdelg/rl4o/org.rl4o.provider)
* [Source Code](https://gitlab.com/cdelg/rl4o/org.rl4o.provider) (clone with `scm:git:git@gitlab.com:cdelg/rl4o.git/org.rl4o.provider`)

## Coordinates

### Maven

```xml
<dependency>
    <groupId>org.rl4o</groupId>
    <artifactId>org.rl4o.provider</artifactId>
    <version>0.1.0-SNAPSHOT</version>
</dependency>
```

### OSGi

```
Bundle Symbolic Name: org.rl4o.provider
Version             : 0.1.0.201910240802
```

## Components

### org.rl4o.provider.algo.argmax.BasicArgmaxAlgo - *state = enabled, activation = delayed*

#### Services - *scope = singleton*

|Interface name |
|--- |
|org.rl4o.api.algo.RLAlgo |

#### Properties

|Name |Type |Value |
|--- |--- |--- |
|epsilon |Double |0.0 |
|algo.name |String |"basic-argmax" |

#### Configuration - *policy = require*

##### Factory Pid: `org.rl4o.provider.algo.argmax.BasicArgmaxAlgo`

|Attribute |Value |
|--- |--- |
|Id |`epsilon` |
|Required |**true** |
|Type |**Double** |
|Default |0.0 |

---

### org.rl4o.provider.environment.bandit.NormalKArmedBanditEnvironment - *state = enabled, activation = delayed*

#### Services - *scope = prototype*

|Interface name |
|--- |
|org.rl4o.api.environment.RLEnvironment |

#### Properties

|Name |Type |Value |
|--- |--- |--- |
|k |Integer |10 |
|environment.name |String |"normal-k-armed-bandit" |

#### Configuration - *policy = require*

##### Factory Pid: `org.rl4o.provider.environment.bandit.NormalKArmedBanditEnvironment`

|Attribute |Value |
|--- |--- |
|Id |`k` |
|Required |**true** |
|Type |**Integer** |
|Default |10 |

---

### org.rl4o.provider.worker.Worker - *state = enabled, activation = immediate*

#### Services - *scope = singleton*

|Interface name |
|--- |
|org.rl4o.provider.worker.Worker |

#### Properties

No properties.

#### Configuration - *policy = optional*

##### Pid: `org.rl4o.provider.worker.Worker`

No information available.

## Licenses

**Apache License, Version 2.0**
  > The Apache License, Version 2.0
  >
  > For more information see [http://www.opensource.org/licenses/apache2.0.php](http://www.opensource.org/licenses/apache2.0.php).

## Copyright

rl4o

---
Rl4o - [https://gitlab.com/cdelg/rl4o](https://gitlab.com/cdelg/rl4o)