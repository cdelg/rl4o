package org.rl4o.api.algo;

import java.util.function.Function;
import java.util.function.Supplier;
import org.osgi.annotation.versioning.ProviderType;
import org.osgi.util.promise.Promise;

@ProviderType
public interface RLAlgo
{
  Promise<Void> solve(Supplier<Integer> sensor, Function<Integer, Double> effector, int timeSteps);

  AutoCloseable solve(Supplier<Integer> sensor, Function<Integer, Double> effector);
}
