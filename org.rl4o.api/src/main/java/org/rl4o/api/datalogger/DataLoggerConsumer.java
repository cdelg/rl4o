package org.rl4o.api.datalogger;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface DataLoggerConsumer
{

  void close();
}
