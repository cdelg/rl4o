package org.rl4o.api.datalogger;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface DataLoggerBuilder
{
  DataLoggerBuilder withName(String name);

  DataLoggerBuilder addColumn(Class<?> dataType);

  DataLoggerBuilder addColumn(Class<?> dataType, String label);

  DataLogger build();
}
