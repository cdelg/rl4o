package org.rl4o.api.datalogger;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface DataLogger
{
  void log(Object... data);
}
