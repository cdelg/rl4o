package org.rl4o.api.environment;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface RLEnvironment
{
  Integer getState();

  Double execute(Integer action);
}
