package org.rl4o.api.environment;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.osgi.service.component.annotations.ComponentPropertyType;

@ComponentPropertyType
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface EnvironmentName
{
  String value();
}
