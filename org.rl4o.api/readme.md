# Rl4o API

Define the Rl4o API.

## Links

* [Documentation](https://gitlab.com/cdelg/rl4o/org.rl4o.api)
* [Source Code](https://gitlab.com/cdelg/rl4o/org.rl4o.api) (clone with `scm:git:git@gitlab.com:cdelg/rl4o.git/org.rl4o.api`)

## Coordinates

### Maven

```xml
<dependency>
    <groupId>org.rl4o</groupId>
    <artifactId>org.rl4o.api</artifactId>
    <version>0.1.0-SNAPSHOT</version>
</dependency>
```

### OSGi

```
Bundle Symbolic Name: org.rl4o.api
Version             : 0.1.0.201910240802
```

## Licenses

**Apache License, Version 2.0**
  > The Apache License, Version 2.0
  >
  > For more information see [http://www.opensource.org/licenses/apache2.0.php](http://www.opensource.org/licenses/apache2.0.php).

## Copyright

rl4o

---
Rl4o - [https://gitlab.com/cdelg/rl4o](https://gitlab.com/cdelg/rl4o)