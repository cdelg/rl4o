# Rl4o Reference Implementation Index

An index containing the Rl4o Reference Implementations

## Links

* [Documentation](https://gitlab.com/cdelg/rl4o/rl4o-impl-index)
* [Source Code](https://gitlab.com/cdelg/rl4o/rl4o-impl-index) (clone with `scm:git:git@gitlab.com:cdelg/rl4o.git/rl4o-impl-index`)

## Licenses

**Apache License, Version 2.0**
  > The Apache License, Version 2.0
  >
  > For more information see [http://www.opensource.org/licenses/apache2.0.php](http://www.opensource.org/licenses/apache2.0.php).

---
Rl4o - [https://gitlab.com/cdelg/rl4o](https://gitlab.com/cdelg/rl4o)