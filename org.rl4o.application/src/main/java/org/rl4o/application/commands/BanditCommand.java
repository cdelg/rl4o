package org.rl4o.application.commands;

import java.lang.reflect.InvocationTargetException;
import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;
import org.rl4o.api.algo.RLAlgo;
import org.rl4o.api.environment.RLEnvironment;
import org.rl4o.application.annotation.Function;
import org.rl4o.application.annotation.Scope;

@Scope("bandit")
@Function({"argmax"})
@Component(service = BanditCommand.class)
public class BanditCommand
{
  @Reference(target = "(environment.name=basic-argmax)", scope = ReferenceScope.PROTOTYPE_REQUIRED)
  private ComponentServiceObjects<RLEnvironment> environment;

  @Reference(target = "(algo.name=basic-argmax)")
  private RLAlgo algo;

  public void argmax() throws InvocationTargetException, InterruptedException
  {
    for (int i = 0; i < 2000; i++)
    {
      final RLEnvironment env = environment.getService(); // TODO K = 10
      algo.solve(env::getState, env::execute, 1000).getValue(); // TODO epsilon 0.0 (greedy)
      environment.ungetService(env);
    }

    for (int i = 0; i < 2000; i++)
    {
      final RLEnvironment env = environment.getService(); // TODO K = 10
      algo.solve(env::getState, env::execute, 1000).getValue(); // TODO epsilon 0.01 (greedy)
      environment.ungetService(env);
    }

    for (int i = 0; i < 2000; i++)
    {
      final RLEnvironment env = environment.getService(); // TODO K = 10
      algo.solve(env::getState, env::execute, 1000).getValue(); // TODO epsilon 0.1 (greedy)
      environment.ungetService(env);
    }

  }
}
