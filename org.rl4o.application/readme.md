# Rl4o Application

The org.rl4o.application application packaging project - using OSGi enRoute R7

## Links

* [Documentation](https://gitlab.com/cdelg/rl4o/org.rl4o.application)
* [Source Code](https://gitlab.com/cdelg/rl4o/org.rl4o.application) (clone with `scm:git:git@gitlab.com:cdelg/rl4o.git/org.rl4o.application`)

## Coordinates

### Maven

```xml
<dependency>
    <groupId>org.rl4o</groupId>
    <artifactId>org.rl4o.application</artifactId>
    <version>0.1.0-SNAPSHOT</version>
</dependency>
```

### OSGi

```
Bundle Symbolic Name: org.rl4o.application
Version             : 0.1.0.201910240802
```

## Components

### org.rl4o.application.commands.BanditCommand - *state = enabled, activation = delayed*

#### Services - *scope = singleton*

|Interface name |
|--- |
|org.rl4o.application.commands.BanditCommand |

#### Properties

|Name |Type |Value |
|--- |--- |--- |
|osgi.command.function |String[] |["argmax"] |
|osgi.command.scope |String |"bandit" |

#### Configuration - *policy = optional*

##### Pid: `org.rl4o.application.commands.BanditCommand`

No information available.

## Licenses

**Apache License, Version 2.0**
  > The Apache License, Version 2.0
  >
  > For more information see [http://www.opensource.org/licenses/apache2.0.php](http://www.opensource.org/licenses/apache2.0.php).

## Copyright

rl4o

---
Rl4o - [https://gitlab.com/cdelg/rl4o](https://gitlab.com/cdelg/rl4o)