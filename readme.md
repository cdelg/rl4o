# Rl4o

This repository provides implementations of reinforcement learning
	 algorithms from the Sutton and Barto 2018 book.

## Links

* [Documentation](https://gitlab.com/cdelg/rl4o)
* [Source Code](https://gitlab.com/cdelg/rl4o) (clone with `scm:git:git@gitlab.com:cdelg/rl4o.git`)

## Built Artifacts

* [**Rl4o API**](org.rl4o.api): Define the Rl4o API.
* [**Rl4o Provider**](org.rl4o.provider): The the Rl4o implementation bundle.
* [**Rl4o Reference Implementation Index**](rl4o-impl-index): An index containing the Rl4o Reference Implementations
* [**Rl4o Application**](org.rl4o.application): The org.rl4o.application application packaging project - using OSGi enRoute R7

## Licenses

**Apache License, Version 2.0**
  > The Apache License, Version 2.0
  >
  > For more information see [http://www.opensource.org/licenses/apache2.0.php](http://www.opensource.org/licenses/apache2.0.php).

---
Rl4o - [https://gitlab.com/cdelg/rl4o](https://gitlab.com/cdelg/rl4o)